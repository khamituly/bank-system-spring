package hb.edu.controller;

import hb.edu.dao.CardDAO;
import hb.edu.dao.OutcomesDAO;
import hb.edu.dao.UserDAO;
import hb.edu.model.Card;
import hb.edu.model.Outcomes;
import hb.edu.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/transaction")
public class TransactionsController {
    private final OutcomesDAO outcomesDAO;
    private final CardDAO cardDAO;
    private final UserDAO userDAO;
    private final Logger logger = Logger.getLogger(String.valueOf(TransactionsController.class));

    @Autowired
    public TransactionsController(OutcomesDAO outcomesDAO,CardDAO cardDAO,UserDAO userDAO){
        this.outcomesDAO = outcomesDAO;
        this.cardDAO = cardDAO;
        this.userDAO = userDAO;
    }

    @PostMapping()
    public String transferMoney(@RequestParam("cardFrom") int cardFrom,
                              @RequestParam("cardTo") int cardTo,
                              @RequestParam("amount") int amount
                                 ) throws SQLException {
        logger.info("Service Request: tranaction from "+cardFrom+" to "+cardTo);
        System.out.println(cardFrom);
        Card card = cardDAO.getCardByCardNum(cardFrom);
        User user = userDAO.getUserById(card.getId());
        if(cardDAO.transferMoney(card,cardTo,amount,user)){
            outcomesDAO.setOutcomes(new Outcomes(user.getUserId(),
                                        card.getCard(),
                                        cardTo,
                                        amount));
        }

        return "redirect:/transaction/goProfile?id="+user.getUserId();

    }

    @GetMapping("/goProfile")
    private String goProfile(Model model,@RequestParam("id") int id) throws SQLException {
        logger.info("Service Request: entering profile ");

        User user = userDAO.getUserById(id);
        List<Card> cards = cardDAO.getCardsByUserId(user.getUserId());
        if(user == null){
            return "login";
        }else{
            model.addAttribute("user",user);
            model.addAttribute("cardList",cards);
            return "profile";
        }
    }

    @GetMapping("/history/{id}")
    public String getHistory(@PathVariable("id") int id,Model model) throws SQLException {
        User user = userDAO.getUserById(id);
        List<Outcomes> history = outcomesDAO.getHistoryById(id);
        model.addAttribute("history", history);
        model.addAttribute("user",user);
        return "history";
    }
}
