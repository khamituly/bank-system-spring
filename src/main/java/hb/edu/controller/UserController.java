package hb.edu.controller;

import hb.edu.dao.CardDAO;
import hb.edu.dao.UserDAO;
import hb.edu.model.Card;
import hb.edu.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping("/user")
public class UserController  {
    private final UserDAO userDAO;
    private final CardDAO cardDAO;
    private final static Logger logger = Logger.getLogger(String.valueOf((UserController.class)));

    @Autowired
    public UserController(UserDAO userDAO,CardDAO cardDAO){
        this.userDAO = userDAO;
        this.cardDAO = cardDAO;
    }

    @GetMapping
    public String Login(){
        return "login";
    }

    @PostMapping
    public String Login(Model model, @RequestParam("email") String email, @RequestParam("password") String pass) throws SQLException {
        logger.info("Service Request: login to profile "+email);
        User user = userDAO.authenticateUser(email,pass);
        List<Card> cards = cardDAO.getCardsByUserId(user.getUserId());

        if(user == null){
            return "login";
        }else{
            model.addAttribute("user",user);
            model.addAttribute("cardList",cards);
            return "profile";
        }
    }

    @GetMapping("/profile")
    public String Login(Model model,@RequestParam("id") int id) throws SQLException {
        logger.info("Service Request: login to profile ");
        User user = userDAO.getUserById(id);
        List<Card> cards = cardDAO.getCardsByUserId(user.getUserId());

        if(user == null){
            return "login";
        }else{
            model.addAttribute("user",user);
            model.addAttribute("cardList",cards);
            return "profile";
        }
    }

    @PostMapping("/new")
    public String create(@ModelAttribute("user") @Valid User user,
                         BindingResult bindingResult) throws SQLException {
        if (bindingResult.hasErrors())
            return "registration";

        userDAO.save(user);
        return "redirect:/user/profile?id="+user.getUserId();
    }


    @PostMapping("/edit/{id}")
    public String update(@ModelAttribute("user") @Valid User user,
                         @PathVariable("id") int id,
                         BindingResult bindingResult) throws SQLException {
        if (bindingResult.hasErrors())
            return "edit";

        userDAO.update(user,id);
        System.out.println(user.getUserId());
        return "redirect:/user/profile?id="+user.getUserId();
    }

    @GetMapping("/edit/{id}")
    public String update(Model model,
                         @PathVariable("id") int id) throws SQLException {
        model.addAttribute("user",userDAO.getUserById(id));
        return "edit";
    }

    @GetMapping("/exit")
    public String exit(){
        return "login";
    }

    @GetMapping("/reg")
    public String registration(@ModelAttribute("user") User user){
        return "registration";
    }



}
