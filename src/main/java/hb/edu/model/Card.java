package hb.edu.model;

public class Card {
    private int id;
    private int card;
    private double balance;

    public Card(){}

    public Card(int id, int card, double balance) {
        this.id = id;
        this.card = card;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCard() {
        return card;
    }

    public void setCard(int card) {
        this.card = card;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
