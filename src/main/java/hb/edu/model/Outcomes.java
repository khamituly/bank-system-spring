package hb.edu.model;

public class Outcomes {
    private int userId;
    private int from;
    private int to;
    private double balance;

    public Outcomes(int userId, int from, int to, double balnce) {
        this.userId = userId;
        this.from = from;
        this.to = to;
        this.balance = balnce;
    }

    public int getId() {
        return userId;
    }

    public void setId(int id) {
        this.userId = id;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balnce) {
        this.balance = balnce;
    }


}
