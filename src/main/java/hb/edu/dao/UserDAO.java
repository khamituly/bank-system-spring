package hb.edu.dao;

import hb.edu.config.DB;
import hb.edu.model.User;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserDAO {
    private final DB db;
    private static Connection connection;

    public UserDAO() {
        this.db = new DB();
        connection = this.db.getConnection();
    }

    public User getUserByEmail(String email) throws SQLException {
        String SQL = "SELECT*FROM users WHERE email = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1, email);
        User user = new User();
        ResultSet res = stm.executeQuery();
        while (res.next()) {
            user = new User(
                    res.getInt("id"),
                    res.getString("name"),
                    res.getInt("age"),
                    res.getString("email"),
                    res.getString("password")
            );
        }
        return user;
    }

    public User getUserById(int id) throws SQLException {
        String SQL = "SELECT*FROM users WHERE id = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1, id);
        User user = new User();
        ResultSet res = stm.executeQuery();
        while (res.next()) {
            user = new User(
                    res.getInt("id"),
                    res.getString("name"),
                    res.getInt("age"),
                    res.getString("email"),
                    res.getString("password")
            );
        }
        return user;
    }


    public User authenticateUser(String email, String pass) throws SQLException {
        User user = getUserByEmail(email);
        if (user.getPassword().equals(pass)) {
            return user;
        } else {
            return null;
        }
    }

    private void updateUser(User user) {
    }

    private void saveUser(User user) {
    }

    public void save(User user) throws SQLException {
        String SQL = "INSERT INTO users(name,email,age,password) VALUES(?,?,?,?)";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1, user.getName());
        stm.setString(2, user.getEmail());
        stm.setInt(3, user.getAge());
        stm.setString(4, user.getPassword());
        stm.executeUpdate();
    }

    public void update(User user,int id) throws SQLException {
        String SQL = "UPDATE users SET name = ?, age = ?, email=?, password= ? WHERE id = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setString(1,user.getName());
        stm.setInt(2,user.getAge());
        stm.setString(3,user.getEmail());
        stm.setString(4,user.getPassword());
        stm.setInt(5,id);
        stm.executeUpdate();
    }
}
