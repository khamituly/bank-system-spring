package hb.edu.dao;

import hb.edu.config.DB;
import hb.edu.model.Outcomes;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class OutcomesDAO {
    private final DB db;
    private static Connection connection;

    public OutcomesDAO() {
        this.db = new DB();
        connection = this.db.getConnection();
    }

    public void setOutcomes(Outcomes outcomes) throws SQLException {
        String SQL = "INSERT INTO outcomes VALUES(?,?,?,?)";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,outcomes.getId());
        stm.setInt(2,outcomes.getFrom());
        stm.setInt(3,outcomes.getTo());
        stm.setDouble(4,outcomes.getBalance());
        stm.executeUpdate();
    }

    public List<Outcomes> getHistoryById(int id) throws SQLException {
        String SQL = "SELECT*FROM outcomes WHERE id = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,id);
        ResultSet res = stm.executeQuery();
        List<Outcomes> history = new ArrayList<>();
        while(res.next()){
            history.add(new Outcomes(
                     res.getInt("id"),
                     res.getInt("cardFrom"),
                     res.getInt("cardTo"),
                     res.getDouble("balance")
            ));
        }
        return history;
    }

}
