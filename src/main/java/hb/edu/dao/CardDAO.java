package hb.edu.dao;

import hb.edu.config.DB;
import hb.edu.model.Card;
import hb.edu.model.User;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class CardDAO {
    private final DB db;
    private static Connection connection;

    public CardDAO() {
        this.db = new DB();
        connection = this.db.getConnection();
    }

    public List<Card> getCardsByUserId(int id) throws SQLException {
        String SQL = "SELECT*FROM cards WHERE id = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,id);
        ResultSet res = stm.executeQuery();
        List<Card> cards = new ArrayList<>();
        while (res.next()){
            cards.add(new Card(
                    res.getInt("id"),
                    res.getInt("card"),
                    res.getDouble("balance")
                    )
                );
        }
        return cards;
    }

    public Card getCardByCardNum(int cardNum) throws SQLException {
        String SQL = "SELECT*FROM cards WHERE card = ?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setInt(1,cardNum);
        ResultSet res = stm.executeQuery();
        Card card = null;
        while(res.next()){
            card = new Card(
                    res.getInt("id"),
                    res.getInt("card"),
                    res.getDouble("balance")
            );
        }
        return card;
    }

    public boolean isUserCard(int userId,int cardNum) throws SQLException {
        Card card = getCardByCardNum(cardNum);
        if(userId == card.getId()) {
            return true;
        }else{
            return false;
        }
    }

    public boolean transferMoney(Card card, int cardTo, int amount, User user) throws SQLException {
        double percent = amount/100;
        double toAnotherBank =amount+percent;
        double balance = card.getBalance();
        if(isUserCard(user.getUserId(),cardTo)){
            if(card.getBalance() >= amount) {
                card.setBalance(balance - amount);
                Card recipient = getCardByCardNum(cardTo);
                balance = recipient.getBalance();
                recipient.setBalance(balance+amount);
                update(recipient);
            }else return false;
        }else{
            if(card.getBalance() >= amount+percent){
                card.setBalance(balance-toAnotherBank);
                Card recipient = getCardByCardNum(cardTo);
                balance = recipient.getBalance();
                recipient.setBalance(balance+amount);
                update(recipient);
            }else return false;
        }

        update(card);
        return true;
    }

    public void update(Card card) throws SQLException {
        String SQL = "UPDATE cards SET balance=? WHERE card=?";
        PreparedStatement stm = connection.prepareStatement(SQL);
        stm.setDouble(1,card.getBalance());
        stm.setInt(2,card.getCard());
        stm.executeUpdate();
    }

}
